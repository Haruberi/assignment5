# Assignment5-Java_RPGCharacters
Create a RPG-game.


## Description

Used IntelliJ to create packages with classes and with methods to desribe the following
- com.company(package)
    - Main (class)
    - PrimaryAttribute (class) for primary attributes

- characters(package)
Package with superclass CharacterClass and its subclasses
    - CharacterClass(superclass)
    - Mage (subclass)
    - Ranger (subclass)
    - Rogue (subclass)
    - Warrior (subclass)

- exceptions(package) 
Package with customs exceptions
    - InvalidArmorException
    - InvalidWeaponException

- items(package)
    - Item - (superclass)
    - Slot - (enum) for Slots
    - Attacks - (interface) for DPS methods

- armors(package)
    - Armor (subclass) of Item
    - ArmorType (enum) for armor types
    
- weapons(package)
    - Armor (subclass) of Item
    - WeaponTypes (enum) for weapon types

## Getting started

### Dependencies
- [NPM/Node.js (LTS - Long Term Support Version)](https://nodejs.org/en/)
- [Vue-3 setup](https://v3.vuejs.org/api/sfc-script-setup.html#sfc-script-setup)
- [Volar](https://marketplace.visualstudio.com/items?itemName=johnsoncodehk.volar)
- Vue CLI
- [Vuex](https://vuex.vuejs.org/#what-is-a-state-management-pattern)
- Vue-Router
- [Tailwind](https://tailwindcss.com/)
- [Visual Studio Code](https://code.visualstudio.com/)

### Installing

The project:
- [Assignment1_Java_RPGCharacters](https://gitlab.com/Haruberi/assignment5)


### Executing program

To run program
```
shift + F10
```
Follow instructions in console

## Help

## Authors

Anna Hallberg [@Haruberi](https://gitlab.com/Haruberi/)

## License
No license.

## Acknowledgments
- [readme-remplate](https://gist.github.com/DomPizzie/7a5ff55ffa9081f2de27c315f5018afc)
